import model HAHM_variableMW_UFO
define f = u d c s b t u~ d~ c~ s~ b~ t~ e- e+ mu- mu+ ta- ta+ ve vm vt ve~ vm~ vt~
generate zp > f f
output ZD_WIDTH_EPSILON_SCAN
launch
set mzdinput = 3.000000e+01
set mhsinput = 2.000000e+01
set kap = 1.000000e-02
set axm1 = 1.279000e+02
set epsilon scan:[2.000000e-10,4.000000e-10,6.000000e-10,8.000000e-10,1.000000e-09,2.000000e-09,4.000000e-09,6.000000e-09,8.000000e-09,1.000000e-08]

