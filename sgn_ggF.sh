import model HAHM_variableMW_UFO
define f = u d c s b t u~ d~ c~ s~ b~ t~ e- e+ mu- mu+ ta- ta+ ve vm vt ve~ vm~ vt~
define p = p b b~
generate g g > h, (h > zp zp, zp > f f) #full process w/decay syntax
#generate g g > h #just core process
output ggf_mzd20_eps_m10_100k
launch -i
multi_run 10
shower=Pythia8
detector=Delphes
analysis=OFF
madspin=OFF
done
set mzdinput = 2.000000e+01 #testing from 100 to 30 in steps of 20
set mhsinput = 2.000000e+01
set kap = 1.000000e-02
set axm1 = 1.279000e+02
#set epsilon = 8.000000e-09 #ID
#set epsilon = 4.500000e-09 #EMCal
#set epsilon = 1.000000e-08 #HadCal
#set epsilon = 2.000000e-09 #MS
set epsilon = 1.000000e-10
set wzp Auto
set whs Auto
set nevents=10000
./ggf_mzd20_eps_m10_100k/Cards/delphes_card_ATLAS.dat
#./madspin_card_zpdecay.dat
done
exit
exit
