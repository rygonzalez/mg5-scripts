import model HAHM_variableMW_UFO
define f = u d c s b t u~ d~ c~ s~ b~ t~ e- e+ mu- mu+ ta- ta+ ve vm vt ve~ vm~ vt~
define p = u u~ d d~ c c~ s s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
check p p > h p p QCD=0 QED=3 $$ p a z zp h h2 w+ w-
#generate p p > h p p QCD=0 QED=3 $$ p a z zp h h2 w+ w-, (h > zp zp, zp > l+ l-) #full process w/ decay syntax
output check_vbf_syntax
#display diagrams
