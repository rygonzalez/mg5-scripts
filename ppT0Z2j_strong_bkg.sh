define p = p b b~
define j = p
define nu = ve vm vt ve~ vm~ vt~
generate p p > z j j
output bkg_Zstrong_multirun_10
launch -i
multi_run 10
shower=Pythia8
detector=Delphes
analysis=OFF
madspin=ON
done
set nevents=10000
set maxjetflavor=5
set JetMatching:nJetMax=2
set ickkw=1
./madspin_card_Zstrong.dat
./bkg_Zstrong_multirun_10/Cards/delphes_card_ATLAS.dat
done
exit
exit
