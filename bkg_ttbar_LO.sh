define p = p b b~
generate p p > t t~
output bkg_ttbar_LO_100k
launch -i
multi_run 10
shower=Pythia8
detector=Delphes
analysis=OFF
madspin=ON
done
set nevents=10000
./bkg_ttbar_LO_100k/Cards/delphes_card_ATLAS.dat
#./madspin_card_zpdecay.dat #for ttbar, the default card already contains decay info
done
exit
exit
