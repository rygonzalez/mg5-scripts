# MG5 scripts

Collection of MadGraph5_aMC@NLO scripts for different purposes.

### How to run a script

Having already MG5 installed, run any script by doing
```
./bin/mg5_aMC name_of_the_script.sh
```
