define p = p b b~
define j = p
generate p p > j j @0
add process p p > j j j @1
add process p p > j j j j @2
output bkg_multijet_multirun_10
launch -i
multi_run 10
shower=Pythia8
detector=Delphes
analysis=OFF
done
set nevents=10000
set maxjetflavor=5
set JetMatching:nJetMax=4
set ickkw=1
./bkg_multijet_multirun_10/Cards/delphes_card_ATLAS.dat
done
exit
exit
